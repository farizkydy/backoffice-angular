import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { from, Observable, of } from "rxjs";
import { EmployeeData, EmployeeDataResponse, EmployeeResponse } from "src/@core/domain/employee.entity";
import { EmployeeRepository } from "src/@core/repository/employee.repository";
import { CreateEmployeeDto, GetAllDataEmployeesDto } from "src/@core/usecase/interface/employee.interface";

const baseUrl = 'https://6580f9853dfdd1b11c424344.mockapi.io/rakamin/employee';
@Injectable({
    providedIn: 'root',
})

export class EmployeeAdapter extends EmployeeRepository {
    constructor(private http: HttpClient) { 
        super();
    }
    GetAllDataEmployee(params: GetAllDataEmployeesDto): Observable<EmployeeData[]> {
        return this.http.get<EmployeeData[]>(`${baseUrl}${params.username ? `?username=${params.username}` : ''}`);
    }



    GetDataEmployee(id: string): Observable<EmployeeData> {
        return this.http.get<EmployeeData>(`${baseUrl}/${id}`)
    }

    CreateDataEmployee(payload: CreateEmployeeDto): Observable<EmployeeResponse> {
        return this.http.post<EmployeeResponse>(baseUrl, payload)
    }

    DeleteDataEmployee(id: string): Observable<EmployeeResponse> {
        return this.http.delete<EmployeeResponse>(`${baseUrl}/${id}`)
    }

    UpdateDataEmployee(payload: CreateEmployeeDto): Observable<EmployeeResponse> {
        return this.http.put<EmployeeResponse>(`${baseUrl}/${payload.id}`, payload);
    }
}