import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { EmployeeRepository } from 'src/@core/repository/employee.repository';
import { EmployeeAdapter } from './employee/employee.adapter';


const DATA_SERVICE: Provider[] = [
  { provide: EmployeeRepository, useClass: EmployeeAdapter }
];

@NgModule({
  declarations: [],
  imports: [CommonModule]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [...DATA_SERVICE]
    };
  }
}