import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UseCase } from 'src/base/use-case';
import { EmployeeData } from '../domain/employee.entity';
import { EmployeeRepository } from '../repository/employee.repository';

@Injectable({
    providedIn: 'root'
})

export class GetDetailDataEmployeeUseCase implements UseCase<string, EmployeeData> {
    constructor (private employeeRepository: EmployeeRepository) {}

    execute(payload: string): Observable<EmployeeData> {
        return this.employeeRepository.GetDataEmployee(payload)
    }
}