import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UseCase } from 'src/base/use-case';
import { EmployeeResponse } from '../domain/employee.entity';
import { EmployeeRepository } from '../repository/employee.repository';
import { CreateEmployeeDto } from './interface/employee.interface';

@Injectable({
    providedIn: 'root'
})

export class UpdateDataEmployeeUseCase implements UseCase<CreateEmployeeDto, EmployeeResponse> {
    constructor (private employeeRepository: EmployeeRepository) {}

    execute(payload: CreateEmployeeDto): Observable<EmployeeResponse> {
        return this.employeeRepository.UpdateDataEmployee(payload)
    }
}