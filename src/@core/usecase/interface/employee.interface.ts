export interface CreateEmployeeDto {
    id?: string;
    name: string;
    birthDate: string;
    email: string;
    basicSalary: string;
    group: string;
}

export interface GetAllDataEmployeesDto {
    username?: string;
    page?: number;
    totalData?: number;
}