import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UseCase } from 'src/base/use-case';
import { EmployeeData } from '../domain/employee.entity';
import { EmployeeRepository } from '../repository/employee.repository';
import { GetAllDataEmployeesDto } from './interface/employee.interface';

@Injectable({
    providedIn: 'root'
})

export class GetAllDataEmployeeUseCase implements UseCase<GetAllDataEmployeesDto, EmployeeData[]> {
    constructor (private employeeRepository: EmployeeRepository) {}

    execute(params: GetAllDataEmployeesDto): Observable<EmployeeData[]> {
        return this.employeeRepository.GetAllDataEmployee(params)
    }
}