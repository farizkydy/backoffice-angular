import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UseCase } from 'src/base/use-case';
import { EmployeeResponse } from '../domain/employee.entity';
import { EmployeeRepository } from '../repository/employee.repository';

@Injectable({
    providedIn: 'root'
})

export class DeleteDataEmployeeUseCase implements UseCase<string, EmployeeResponse> {
    constructor (private employeeRepository: EmployeeRepository) {}

    execute(payload: string): Observable<EmployeeResponse> {
        return this.employeeRepository.DeleteDataEmployee(payload)
    }
}