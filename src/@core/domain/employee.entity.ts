export interface EmployeeDataResponse {
    employee: EmployeeData[];
}

export interface EmployeeData {
    id: string;
    username:  string;
    firstName: string;
    lastName: string;
    email: string;
    birthDate: Date;
    basicSalary: number;
    status: string;
    group: string;
    description: string
}

export interface EmployeeResponse {
    status: number;
    message: string;
}