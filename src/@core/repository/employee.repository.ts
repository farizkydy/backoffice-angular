import { Observable } from 'rxjs';
import { EmployeeData, EmployeeDataResponse, EmployeeResponse } from '../domain/employee.entity';
import { CreateEmployeeDto, GetAllDataEmployeesDto } from '../usecase/interface/employee.interface';

export abstract class EmployeeRepository {
    abstract GetAllDataEmployee(params: GetAllDataEmployeesDto): Observable<EmployeeData[]>;
    abstract GetDataEmployee(id: string): Observable<EmployeeData>;
    abstract CreateDataEmployee(payload: CreateEmployeeDto): Observable<EmployeeResponse>;
    abstract UpdateDataEmployee(payload: CreateEmployeeDto): Observable<EmployeeResponse>;
    abstract DeleteDataEmployee(id: string): Observable<EmployeeResponse>;
}