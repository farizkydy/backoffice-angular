import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-employee-pagination',
  templateUrl: './employee-pagination.component.html',
  styleUrls: ['./employee-pagination.component.css']
})
export class EmployeePaginationComponent {

  @Input() currentPage: number = 1;
  @Input() totalPages: number = 1;
  @Input() totalPagesArray: number[] = [1, 2]
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  onPageChange(page: number) {
    this.pageChange.emit(page);
    console.log(page)
  }

}
