import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeItemsPerPageComponent } from './employee-items-per-page.component';

describe('EmployeeItemsPerPageComponent', () => {
  let component: EmployeeItemsPerPageComponent;
  let fixture: ComponentFixture<EmployeeItemsPerPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeItemsPerPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmployeeItemsPerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
