import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-employee-items-per-page',
  templateUrl: './employee-items-per-page.component.html',
  styleUrls: ['./employee-items-per-page.component.css']
})
export class EmployeeItemsPerPageComponent {

  @Input() itemsPerPageOptions: number[] = [];
  @Input() itemsPerPage: number = 0;
  @Output() itemsPerPageChange: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  onItemsPerPageChange(event: any) {
    this.itemsPerPageChange.emit(parseInt(event.target.value));
  }

}
