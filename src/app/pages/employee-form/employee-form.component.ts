import { group } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeData } from 'src/@core/domain/employee.entity';
import { CreateDataEmployeeUseCase } from 'src/@core/usecase/create-employee.usecase';
import { GetDetailDataEmployeeUseCase } from 'src/@core/usecase/get-detail-employee.usecase';
import { CreateEmployeeDto } from 'src/@core/usecase/interface/employee.interface';
import { UpdateDataEmployeeUseCase } from 'src/@core/usecase/update-employee.usecase';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
  routeValue: string = '';
  formData!: FormGroup;
  groupNames: string[] = [
    'Group 1',
    'Group 2',
    'Group 3',
    'Group 4',
    'Group 5',
    'Group 6',
    'Group 7',
    'Group 8',
    'Group 9',
    'Group 10'
  ];
  selectedGroup: string = '';
  maxDate: string = new Date().toISOString().split('T')[0];
  employeeData!: EmployeeData;
  employeeId: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private createDataEmployeeUseCase: CreateDataEmployeeUseCase,
    private getDetailDataEmployeeUseCase: GetDetailDataEmployeeUseCase,
    private updateDataEmployeeUseCase: UpdateDataEmployeeUseCase,
    public route: Router
  ) { 
    this.routeValue = this.activatedRoute.snapshot.url[0].path;
    this.activatedRoute.params.subscribe(params => {
      this.employeeId = params['id'];
    })
  }

  ngOnInit(): void {
    this.initForm();
    if (this.routeValue === 'edit') {
      this.getEmployeeData();
    }
  }

  initForm() {
    this.formData = this.formBuilder.group({
      name: ['', Validators.required],
      birthdate: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      basicSalary: ['', Validators.required],
      group: [this.groupNames[0], Validators.required]
    });
  }

  onSubmit() {
    if (this.routeValue === 'add') {
      this.saveData();
    } else {
      this.editData();
    }

  }

  saveData() {
    const payload: CreateEmployeeDto = {
      name: this.formData.get('name')?.value,
      birthDate: this.formData.get('birthdate')?.value,
      email: this.formData.get('email')?.value,
      basicSalary: this.formData.get('basicSalary')?.value,
      group: this.formData.get('group')?.value
    }
    this.createDataEmployeeUseCase.execute(payload).subscribe(() => {
      this.route.navigate(['']);
    })
  }

  getEmployeeData() {
    this.getDetailDataEmployeeUseCase.execute(this.employeeId).subscribe((res) => {
      this.employeeData = res;
      this.setFormValue(this.employeeData);
    })
  }

  setFormValue(value: any) {
    this.formData.get('name')?.setValue(value.username);
    this.formData.get('birthdate')?.setValue(value.birthdate);
    this.formData.get('email')?.setValue(value.email);
    this.formData.get('basicSalary')?.setValue(value.basicSalary);
    this.formData.get('group')?.setValue(value.group);
  }

  editData() {
    const payload: CreateEmployeeDto = {
      id: this.employeeId,
      name: this.formData.get('name')?.value,
      birthDate: this.formData.get('birthdate')?.value,
      email: this.formData.get('email')?.value,
      basicSalary: this.formData.get('basicSalary')?.value,
      group: this.formData.get('group')?.value
    }
    this.updateDataEmployeeUseCase.execute(payload).subscribe(() => {
      this.route.navigate([''])
    })
  }

}
