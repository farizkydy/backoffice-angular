import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeData } from 'src/@core/domain/employee.entity';
import { DeleteDataEmployeeUseCase } from 'src/@core/usecase/delete-employee.usecase';
import { GetDetailDataEmployeeUseCase } from 'src/@core/usecase/get-detail-employee.usecase';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {
  employeeData!: EmployeeData;
  employeeId: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private getDetailDataEmployeeUseCase: GetDetailDataEmployeeUseCase,
    public route: Router,
    private deleteDataEmployeeUseCase: DeleteDataEmployeeUseCase,
  ) { 
    this.activatedRoute.params.subscribe(params => {
      this.employeeId = params['id'];
    })
  }

  ngOnInit(): void {
    this.getEmployeeData();
  }

  getEmployeeData() {
    this.getDetailDataEmployeeUseCase.execute(this.employeeId).subscribe((res) => {
      this.employeeData = res;
    })
  }

  deleteEmployee(id: string) {
    this.deleteDataEmployeeUseCase.execute(id).subscribe(res => {
      this.route.navigate(['']);
    })
  }

  editEmployee(id: string) {
    this.route.navigate([`edit/${id}`]);
  }

}
