import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeData } from 'src/@core/domain/employee.entity';
import { DeleteDataEmployeeUseCase } from 'src/@core/usecase/delete-employee.usecase';
import { GetAllDataEmployeeUseCase } from 'src/@core/usecase/get-all-employee.usecase';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  @ViewChild('deleteToast') deleteToast!: ElementRef;

  employee: EmployeeData[] = [];
  filteredEmployees: EmployeeData[] = [];
  title = '';
  filtering = {
    username: '',
    currentPage: 1,
    itemsPerPage: 50,
    itemsPerPageOptions: [25, 50, 100],
    totalPagesArray: [1, 2]
  }
  isDeletToast = false;

  constructor( 
    private getAllDataEmployeeUseCase: GetAllDataEmployeeUseCase,
    private deleteDataEmployeeUseCase: DeleteDataEmployeeUseCase,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.getAllData();
  }

  getAllData(): void {
    this.getAllDataEmployeeUseCase.execute(this.filtering)
      .subscribe({
        next: (data) => {
          this.employee = data;
          this.applyFilter()
        },
        error: (e) => console.error(e)
      });
  }

  applyFilter() {
    this.filteredEmployees = this.employee.filter(employee =>
      employee.username.toLowerCase().includes(this.filtering.username.toLowerCase())
    );
    this.filtering.currentPage = 1;
    this.onItemsPerPageChange(this.filtering.itemsPerPage)
  }

  get paginatedEmployees(): EmployeeData[] {
    const startIndex = (this.filtering.currentPage - 1) * this.filtering.itemsPerPage;
    return this.employee.slice(startIndex, startIndex + this.filtering.itemsPerPage);
  
  }
  onPageChange(page: number) {
    this.filtering.currentPage = page;
  }

  onItemsPerPageChange(itemsPerPage: number) {
    this.filtering.itemsPerPage = itemsPerPage;
    this.filtering.currentPage = 1;
    const totalItems = this.filteredEmployees.length;
    const totalPages = Math.ceil(totalItems / itemsPerPage);
    this.filtering.totalPagesArray = Array.from({ length: totalPages }, (_, index) => index + 1);

  }

  deleteData(id: string) {
    this.deleteDataEmployeeUseCase.execute(id).subscribe(res => {
      alert('Success Deleted Data')
      this.getAllData();
    })
  }

  editData(id: string) {
    this.router.navigate([`edit/${id}`]);
  }
  
  detailPage(id: string) {
    this.router.navigate([`employee/${id}`]);
  }

}